package com.matusmahut.isometricRPG.map;

import java.util.ArrayList;
import java.util.LinkedList;
 

public class PathFinder {
 
    private static final int maxSteps = 200;
 
    private static Segment[][] map;
    public static void setArea(Segment[][] area) {
        map = area;
    }
 
    private static ArrayList<Segment> openList = new ArrayList<Segment>();
    private static ArrayList<Segment> closedList =  new ArrayList<Segment>();
    private static LinkedList<Segment> calculatedPath = new LinkedList<Segment>();
    
 
    public static LinkedList<Segment> calculatePath(Segment start, Segment target) {
        //Pred vypoctom cesty vycisti oba listy, v pripade ze v nich bolo nieco z predchadzajucich vypoctov
        openList.clear();
        closedList.clear();
        calculatedPath = new LinkedList<Segment>();
        int stepCounter = 0;
 
        //Aktualny prehladavany segment
        Segment currentSegment = null;
 
        //ak je cielovy segment prekazka alebo nieje ziadny cielovy segment, cesta neexistuje
        if (start == null || target == null || target.isBlocked()) {
            return null;
        }
 
        //inicializacia hodnot pociatocneho segmentu
        //kedze sa uz postava na segmente nachadza, cena jeho prejdenia je 0
        start.setCost(0);
 
        //NEPAMATAM SI PRESNE PRECO SA TO TAKTO POCITA, TREBA DOSTUDOVAT Z TEORIE O A*
        start.setDistance((Math.abs(target.getXIndex() - start.getXIndex()) + Math.abs(target.getYIndex() - start.getYIndex())) * 10);
 
        //Pridame pociatocny segment do open listu, co su segmenty ktore sa budu kontrolovat
        openList.add(start);
 
        //dalsie operacie vykonavame, pokial ma zmysel este prehladavat nejake segmenty,
        //cize pokial sa v Open Liste nieco nachadza
        while (!openList.isEmpty()) {
            //increase calculation step counter
            if (stepCounter > maxSteps){
                // no path was found during max allowed steps
                return null;
            }
            stepCounter++;
 
 
            //Aktualnu najkratsiu vzdialenost nastavime na dostatocne vysoku hodnotu
            int minDistance = Integer.MAX_VALUE;
            //najdeme segment s minimalnou hodnotou prejdenia
            for (Segment s : openList) {
                if (s.getF() < minDistance) {
                    //posunieme sa na prvy segment s mensou hodnotou prejdenia
                    currentSegment = s;
                    //aktualizujeme minimalnu vzdialenost
                    minDistance = s.getF();
 
                    //ak stojime v cieli nasli sme cestu
                    if (currentSegment == target) {
                        makePath(target, start);
                        return calculatedPath;
                    }
                }
            }
 
            if (currentSegment == null) {
                return null;
            }
 
 
            //odobereme sucasny uz skontrolovany segment z open listu
            openList.remove(currentSegment);
            //pridame sucasny uz skontrolovany segment do closed listu
            closedList.add(currentSegment);
 
            //skontrolujeme segmenty v okoli Segmentu current segment
            //counter sluzi na urcovanie ci sa jedna o diagonalny pohyb
            int counter = 0;
            for (int x = currentSegment.getXIndex() - 1; x < currentSegment.getXIndex() + 2; x++) {
                for (int y = currentSegment.getYIndex() - 1; y < currentSegment.getYIndex() + 2; y++) {
                    counter++;
                    Segment controlledSegment = null;
                    boolean isDiagonal = false;
                    boolean isClosed = false;
 
                    if (counter == 1 || counter == 3 || counter == 7 || counter == 9) {
                        isDiagonal = true;
                    }
 
                    //skontrolujeme ci sa segment nachadza v closed liste
                    for (Segment s : closedList) {
                        if (x == s.getXIndex() && y == s.getYIndex()) {
                            isClosed = true;
                        }
                    }
 
                    //kontrolujeme ci sa nepokusame prehladavat segment ktory je mimo mapy resp. neexistuje
                    if (x > -1 && y > -1 && x < map.length && y < map[0].length) {
                        //v kontrole pokracujeme iba ak sa segment nenachadza v closed liste a ak nieje prekazkou
                        if (!map[x][y].isBlocked() && !isClosed) {
                            //kontrolujeme ci sa jedna o diagonalny segment
                            if (isDiagonal) {
                                if (y < map[0].length && x < map.length) {
                                    if ((counter == 1 && map[x + 1][y].isBlocked()) || (counter == 1 && map[x][y + 1].isBlocked())) {
                                        continue;
                                    }
                                }
 
                                if (y > 0 && x < map.length) {
                                    if ((counter == 3 && map[x + 1][y].isBlocked()) || (counter == 3 && map[x][y - 1].isBlocked())) {
                                        continue;
                                    }
                                }
 
                                if (y < map[0].length && x > 0) {
                                    if ((counter == 7 && map[x - 1][y].isBlocked()) || (counter == 7 && map[x][y + 1].isBlocked())) {
                                        continue;
                                    }
                                }
 
                                if (y > 0 && x > 0) {
                                    if ((counter == 9 && map[x - 1][y].isBlocked()) || (counter == 9 && map[x][y - 1].isBlocked())) {
                                        continue;
                                    }
                                }
                            }
 
                            boolean isOpen = false;
                            for (Segment s : openList) {
                                if (x == s.getXIndex() && y == s.getYIndex()) {
                                    isOpen = true;
                                    controlledSegment = s;
                                }
                            }
 
                            if (!isOpen) {
                                //vkladanie segmentov do OpenListu
                                if (isDiagonal) {
                                    map[x][y].setCost(currentSegment.getCost() + 14);
                                } else {
                                    map[x][y].setCost(currentSegment.getCost() + 10);
                                }
                                map[x][y].setDistance((Math.abs(target.getXIndex() - x) + Math.abs(target.getYIndex() - y)) * 10);
                                map[x][y].setParent(currentSegment);
                                openList.add(map[x][y]);
                            } else {
 
                                int alternativeG;
 
                                if (isDiagonal) {
                                    alternativeG = currentSegment.getCost() + 14;
                                } else {
                                    alternativeG = currentSegment.getCost() + 10;
                                }
 
                                if ((controlledSegment.getCost() > alternativeG)) {
                                    controlledSegment.setCost(alternativeG);
                                    controlledSegment.setParent(currentSegment);
                                }
 
                            }
                        }
                    }
                }
            }
 
        }
        return null;
    }
 
 
    private static void makePath(Segment paTarget, Segment paStart) {
        calculatedPath.clear();
 
        while (paTarget != paStart) {
            calculatedPath.push(paTarget);
//            calculatedPath.add(paTarget);
            paTarget = paTarget.getParent();
        }
 
        //because of queue I don't need to do collection reverse
        //Collections.reverse(calculatedPath);
    }
 
}